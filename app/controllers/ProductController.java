package controllers;

import models.entity.Product;
import models.interefaces.CreateRequired;
import models.interefaces.UpdateRequired;
import models.repository.ProductRepository;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Manage a database of products
 */
public class ProductController extends Controller {

    private final ProductRepository productRepository;
    private final FormFactory formFactory;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public ProductController(FormFactory formFactory,
                             ProductRepository productRepository,
                             HttpExecutionContext httpExecutionContext) {
        this.productRepository = productRepository;
        this.formFactory = formFactory;
        this.httpExecutionContext = httpExecutionContext;
    }

    /**
     * Display the paginated list of products.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied on product names
     */
    public CompletionStage<Result> list(int page, String sortBy, String order, String filter) {
        // Run a db operation in another thread (using DatabaseExecutionContext)
        return productRepository.page(page, 10, sortBy, order, filter).thenApplyAsync(list -> {
            // This is the HTTP rendering thread context
            return ok(Json.toJson(list));
        }, httpExecutionContext.current());
    }

    /**
     * Method to create a new product
     *
     * @return Json from a Product model added
     */
    public CompletionStage<Result> create() {
        Form<Product> productForm = formFactory.form(Product.class, CreateRequired.class).bindFromRequest();
        if (productForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(productForm.errorsAsJson()));
        }

        Product product = productForm.get();
        return productRepository.insert(product).thenApplyAsync(data -> {
            if (data.isPresent()) {

                return ok(Json.toJson(data));
            } else {
                return badRequest();
            }
        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> update(Long id) throws PersistenceException {
        Form<Product> productForm = formFactory.form(Product.class, UpdateRequired.class).bindFromRequest();
        if (productForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(productForm.errorsAsJson()));
        } else {
            Product product = productForm.get();
            return productRepository.update(id, product).thenApplyAsync(data -> {
                if (data.isPresent()) {

                    return ok(Json.toJson(data));
                } else {
                    return badRequest();
                }
            }, httpExecutionContext.current());
        }
    }

    /**
     * Handle product deletion
     */
    public CompletionStage<Result> delete(Long id) {
        return productRepository.delete(id).thenApplyAsync(v -> {
            return ok(Json.toJson(v));

        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> find(Long id) {
        return productRepository.find(id).thenApplyAsync(data -> {
            return ok(Json.toJson(data));
        }, httpExecutionContext.current());
    }
}
            
