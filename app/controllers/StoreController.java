package controllers;

import models.entity.Store;
import models.interefaces.CreateRequired;
import models.interefaces.UpdateRequired;
import models.repository.StoreRepository;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Manage a database of stores
 */
public class StoreController extends Controller {

    private final StoreRepository storeRepository;
    private final FormFactory formFactory;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public StoreController(FormFactory formFactory,
                           StoreRepository storeRepository,
                           HttpExecutionContext httpExecutionContext) {
        this.storeRepository = storeRepository;
        this.formFactory = formFactory;
        this.httpExecutionContext = httpExecutionContext;
    }

    /**
     * Display the paginated list of stores.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied on store names
     */
    public CompletionStage<Result> list(int page, String sortBy, String order, String filter) {
        // Run a db operation in another thread (using DatabaseExecutionContext)
        return storeRepository.page(page, 10, sortBy, order, filter).thenApplyAsync(list -> {
            // This is the HTTP rendering thread context
            return ok(Json.toJson(list));
        }, httpExecutionContext.current());
    }

    /**
     * Method to create a new store
     *
     * @return Json from a Store model added
     */
    public CompletionStage<Result> create() {
        Form<Store> storeForm = formFactory.form(Store.class, CreateRequired.class).bindFromRequest();
        if (storeForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(storeForm.errorsAsJson()));
        }

        Store store = storeForm.get();
        return storeRepository.insert(store).thenApplyAsync(data -> {
            if (data.isPresent()) {

                return ok(Json.toJson(data));
            } else {
                return badRequest();
            }
        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> update(Long id) throws PersistenceException {
        Form<Store> storeForm = formFactory.form(Store.class, UpdateRequired.class).bindFromRequest();
        if (storeForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(storeForm.errorsAsJson()));
        } else {
            Store store = storeForm.get();
            return storeRepository.update(id, store).thenApplyAsync(data -> {
                if (data.isPresent()) {

                    return ok(Json.toJson(data));
                } else {
                    return badRequest();
                }
            }, httpExecutionContext.current());
        }
    }

    /**
     * Handle store deletion
     */
    public CompletionStage<Result> delete(Long id) {
        return storeRepository.delete(id).thenApplyAsync(v -> {
            return ok(Json.toJson(v));

        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> find(Long id) {
        return storeRepository.find(id).thenApplyAsync(data -> {
            return ok(Json.toJson(data));
        }, httpExecutionContext.current());
    }
}
            
