package controllers;

import converter.PurchaseConverter;
import models.interefaces.CreateRequired;
import models.presentation.Purchase;
import models.presentation.PurchaseDetail;
import models.repository.PurchaseRepository;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Manage a database of purchases
 */
public class PurchaseController extends Controller {

    private final PurchaseRepository purchaseRepository;
    private final FormFactory formFactory;
    private final HttpExecutionContext httpExecutionContext;
    private final PurchaseConverter purchaseConverter;

    @Inject
    public PurchaseController(FormFactory formFactory,
                              PurchaseRepository purchaseRepository,
                              HttpExecutionContext httpExecutionContext,
                              PurchaseConverter purchaseConverter) {
        this.purchaseRepository = purchaseRepository;
        this.formFactory = formFactory;
        this.httpExecutionContext = httpExecutionContext;
        this.purchaseConverter = purchaseConverter;
    }

    /**
     * Display the paginated list of clients.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied on client names
     */
    public CompletionStage<Result> list(int page, String sortBy, String order, String filter) {
        // Run a db operation in another thread (using DatabaseExecutionContext)
        return purchaseRepository.page(page, 10, sortBy, order, filter).thenApplyAsync(list -> {
            // This is the HTTP rendering thread context
            return ok(Json.toJson(list));
        }, httpExecutionContext.current());
    }

    /**
     * Method to create a new purchase with detail
     *
     * @return Json from a Purchase model added
     */
    public CompletionStage<Result> create() {
        Form<Purchase> purchaseForm = formFactory.form(Purchase.class, CreateRequired.class).bindFromRequest();
        if (purchaseForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(purchaseForm.errorsAsJson()));
        }

        models.entity.Purchase purchase = purchaseConverter.presentationToModel(purchaseForm.get());
        return purchaseRepository.insert(purchase).thenApplyAsync(purchaseId ->
        {
            Logger.debug("ResId: " + purchaseId);
            for (PurchaseDetail purchaseDetail : purchaseForm.get().getProducts()) {
                purchaseRepository.insertDetail(purchaseConverter.detailPresentationToModel(purchaseDetail, purchaseId));
            }
            purchase.refresh();
            return ok(Json.toJson(purchase));

        }, httpExecutionContext.current());
    }
}
            
