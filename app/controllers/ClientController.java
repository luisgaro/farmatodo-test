package controllers;

import models.entity.Client;
import models.interefaces.CreateRequired;
import models.interefaces.UpdateRequired;
import models.repository.ClientRepository;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Manage a database of clients
 */
public class ClientController extends Controller {

    private final ClientRepository clientRepository;
    private final FormFactory formFactory;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public ClientController(FormFactory formFactory,
                            ClientRepository clientRepository,
                            HttpExecutionContext httpExecutionContext) {
        this.clientRepository = clientRepository;
        this.formFactory = formFactory;
        this.httpExecutionContext = httpExecutionContext;
    }

    /**
     * Display the paginated list of clients.
     *
     * @param page   Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order  Sort order (either asc or desc)
     * @param filter Filter applied on client names
     */
    public CompletionStage<Result> list(int page, String sortBy, String order, String filter) {
        // Run a db operation in another thread (using DatabaseExecutionContext)
        return clientRepository.page(page, 10, sortBy, order, filter).thenApplyAsync(list -> {
            // This is the HTTP rendering thread context
            return ok(Json.toJson(list));
        }, httpExecutionContext.current());
    }

    /**
     * Method to create a new client
     *
     * @return Json from a Client model added
     */
    public CompletionStage<Result> create() {
        Form<Client> clientForm = formFactory.form(Client.class, CreateRequired.class).bindFromRequest();
        if (clientForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(clientForm.errorsAsJson()));
        }

        Client client = clientForm.get();
        return clientRepository.insert(client).thenApplyAsync(data -> {
            if (data.isPresent()) {

                return ok(Json.toJson(data));
            } else {
                return badRequest();
            }
        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> update(Long id) throws PersistenceException {
        Form<Client> clientForm = formFactory.form(Client.class, UpdateRequired.class).bindFromRequest();
        if (clientForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(clientForm.errorsAsJson()));
        } else {
            Client client = clientForm.get();
            return clientRepository.update(id, client).thenApplyAsync(data -> {
                if (data.isPresent()) {

                    return ok(Json.toJson(data));
                } else {
                    return badRequest();
                }
            }, httpExecutionContext.current());
        }
    }

    /**
     * Handle client deletion
     */
    public CompletionStage<Result> delete(Long id) {
        return clientRepository.delete(id).thenApplyAsync(v -> {
            return ok(Json.toJson(v));

        }, httpExecutionContext.current());
    }

    public CompletionStage<Result> find(Long id) {
        return clientRepository.find(id).thenApplyAsync(data -> {
            return ok(Json.toJson(data));
        }, httpExecutionContext.current());
    }
}
            
