package controllers;

import models.entity.Client;
import models.interefaces.LoginRequired;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import models.repository.ClientRepository;
import models.repository.LoginRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Manage actions of login and clients uses
 */
public class LoginController extends Controller {

    private final LoginRepository loginRepository;
    private final ClientRepository clientRepository;
    private final FormFactory formFactory;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public LoginController(FormFactory formFactory,
                           LoginRepository loginRepository,
                           ClientRepository clientRepository,
                           HttpExecutionContext httpExecutionContext) {
        this.loginRepository = loginRepository;
        this.formFactory = formFactory;
        this.clientRepository = clientRepository;
        this.httpExecutionContext = httpExecutionContext;
    }

    /**
     * Method to login a client with the username and password.
     */
    public CompletionStage<Result> login() {
        Form<Client> loginForm = formFactory.form(Client.class, LoginRequired.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(loginForm.errorsAsJson()));
        }

        return loginRepository.login(loginForm.get()).thenApplyAsync(data -> {
            if (data.isPresent()) {
                return ok(Json.toJson(data));
            } else {
                return unauthorized(Json.toJson("Username or password invalid!"));
            }
        }, httpExecutionContext.current());
    }


}
            
