package converter;

import models.entity.*;

import java.util.Date;

public class PurchaseConverter {

    public Purchase presentationToModel(models.presentation.Purchase purchase) {
        Purchase purchase1 = new Purchase();
        Client client = new Client();
        client.id = purchase.getClientId();
        Store store = new Store();
        store.id = purchase.getStoreId();

        purchase1.client = client;
        purchase1.createDate = new Date();
        purchase1.store = store;

        return purchase1;
    }

    public PurchaseDetail detailPresentationToModel(models.presentation.PurchaseDetail purchase, Long purchaseId) {
        PurchaseDetail purchaseDetail = new PurchaseDetail();
        Product product = new Product();
        product.id = purchase.getProductId();
        Purchase purchase1 = new Purchase();
        purchase1.id = purchaseId;

        purchaseDetail.product = product;
        purchaseDetail.purchase = purchase1;
        purchaseDetail.quantity = purchase.getQuantity();

        return purchaseDetail;
    }
}
