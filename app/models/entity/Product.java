package models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import models.interefaces.CreateRequired;
import models.interefaces.UpdateRequired;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Product entity managed by Ebean
 */
@Entity
@Table(name = "products")
public class Product extends BaseModel {

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "name", nullable = false)
    public String name;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "description")
    public String description;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "barcode", nullable = false, columnDefinition = "TEXT")
    public String barcode;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "price", nullable = false)
    public Double price;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<StoreProducts> products;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<PurchaseDetail> purchaseDetails;

}

