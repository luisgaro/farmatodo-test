package models.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import models.interefaces.CreateRequired;
import models.interefaces.LoginRequired;
import models.interefaces.UpdateRequired;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Client entity managed by Ebean
 */
@Entity
@Table(name = "clients")
public class Client extends BaseModel {

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "name", nullable = false)
    public String name;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "surname", nullable = false)
    public String surname;

    @Constraints.Required(groups ={CreateRequired.class, UpdateRequired.class})
    @Column(name = "birthdate")
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date birthdate;

    @Constraints.Required(groups = CreateRequired.class)
    @Constraints.Email
    @Column(name = "email", nullable = false, unique = true)
    public String email;

    @Constraints.Required(groups = {CreateRequired.class, LoginRequired.class})
    @Column(name = "username", nullable = false, unique = true)
    public String username;

    @Constraints.Required(groups = {CreateRequired.class, LoginRequired.class})
    @Column(name = "password", nullable = false, columnDefinition = "TEXT")
    public String password;

    @Constraints.Required(groups = CreateRequired.class)
    @Column(name = "document_number", nullable = false, unique = true)
    public String documentNumber;

}

