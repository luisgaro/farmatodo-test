package models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import models.interefaces.CreateRequired;
import models.interefaces.UpdateRequired;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Store entity managed by Ebean
 */
@Entity
@Table(name = "stores")
public class Store extends BaseModel {

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "name", nullable = false)
    public String name;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "hours", nullable = false)
    public String hours;

    @Constraints.Required(groups = {CreateRequired.class, UpdateRequired.class})
    @Column(name = "address", nullable = false)
    public String address;

    @OneToMany(mappedBy = "store", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<StoreProducts> products;

}

