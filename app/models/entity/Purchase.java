package models.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import models.interefaces.CreateRequired;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Purchase entity managed by Ebean
 */
@Entity
@Table(name = "purchases")
public class Purchase extends BaseModel {

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "client_id", nullable = false)
    @ManyToOne
    public Client client;

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "store_id", nullable = false)
    @ManyToOne
    public Store store;

    @Constraints.Required(groups = CreateRequired.class)
    @Column(name = "create_date")
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date createDate;

    @OneToMany(mappedBy = "purchase", cascade = CascadeType.ALL)
    public List<PurchaseDetail> purchaseDetails;

}

