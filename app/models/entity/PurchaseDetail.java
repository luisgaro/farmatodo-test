package models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import models.interefaces.CreateRequired;
import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * PurchaseDetail entity managed by Ebean
 */
@Entity
@Table(name = "purchase_detail")
public class PurchaseDetail extends BaseModel {

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "product_id", nullable = false)
    @ManyToOne
    public Product product;

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "purchase_id", nullable = false)
    @ManyToOne
    @JsonBackReference
    public Purchase purchase;

    @Constraints.Required(groups = CreateRequired.class)
    @Column(name = "quantity")
    public Double quantity;

}

