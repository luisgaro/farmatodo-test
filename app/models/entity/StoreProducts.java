package models.entity;

import models.interefaces.CreateRequired;
import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * StoreProducts entity managed by Ebean
 */
@Entity
@Table(name = "store_products")
public class StoreProducts extends BaseModel {

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "product_id", nullable = false)
    @ManyToOne
    public Product product;

    @Constraints.Required(groups = CreateRequired.class)
    @JoinColumn(name = "store_id", nullable = false)
    @ManyToOne
    public Store store;

}

