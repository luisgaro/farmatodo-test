package models.repository;

import io.ebean.DuplicateKeyException;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.PagedList;
import models.entity.Client;
import models.entity.Purchase;
import models.entity.PurchaseDetail;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class PurchaseDetailRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public PurchaseDetailRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }


    public CompletionStage<Optional<PurchaseDetail>> insert(PurchaseDetail purchaseDetail) {
        return supplyAsync(() -> {
            try {
                ebeanServer.insert(purchaseDetail);
                return Optional.of(purchaseDetail);
            } catch (DuplicateKeyException e) {
                return Optional.empty();
            }
        }, executionContext);
    }
}
