package models.repository;

import io.ebean.*;
import models.entity.Product;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class ProductRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public ProductRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    /**
     * Return a paged list of Product
     *
     * @param page     Page to display
     * @param pageSize Number of clients per page
     * @param sortBy   Product property used for sorting
     * @param order    Sort order (either or asc or desc)
     * @param filter   Filter applied on the name column
     */
    public CompletionStage<PagedList<Product>> page(int page, int pageSize, String sortBy, String order, String filter) {
        return supplyAsync(() ->
                ebeanServer.find(Product.class).where()
                        .ilike("name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .setFirstRow(page * pageSize)
                        .setMaxRows(pageSize)
                        .findPagedList(), executionContext);
    }

    public CompletionStage<Optional<Product>> find(Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Product.class).setId(id).findOne()), executionContext);
    }

    public CompletionStage<Optional<Product>> insert(Product client) {
        return supplyAsync(() -> {
            try {
                ebeanServer.insert(client);
                return Optional.of(client);
            } catch (DuplicateKeyException e) {
                return Optional.empty();
            }
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> update(Long id, Product client) {
        return supplyAsync(() -> {
            Transaction txn = ebeanServer.beginTransaction();
            Optional<Long> value = Optional.empty();
            try {
                Product savedProduct = ebeanServer.find(Product.class).setId(id).findOne();
                if (savedProduct != null) {
                    savedProduct.name = client.name;
                    savedProduct.price = client.price;
                    savedProduct.description = client.description;
                    savedProduct.barcode = client.barcode;

                    savedProduct.update();
                    txn.commit();
                    value = Optional.of(id);
                }
            } finally {
                txn.end();
            }
            return value;
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> delete(Long id) {
        return supplyAsync(() -> {
            try {
                final Optional<Product> optionalProduct = Optional.ofNullable(ebeanServer.find(Product.class).setId(id).findOne());
                optionalProduct.ifPresent(Model::delete);
                return optionalProduct.map(c -> c.id);
            } catch (Exception e) {
                return Optional.empty();
            }
        }, executionContext);
    }

}
