package models.repository;

import io.ebean.*;
import models.entity.Client;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class ClientRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public ClientRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    /**
     * Return a paged list of client
     *
     * @param page     Page to display
     * @param pageSize Number of clients per page
     * @param sortBy   Client property used for sorting
     * @param order    Sort order (either or asc or desc)
     * @param filter   Filter applied on the name column
     */
    public CompletionStage<PagedList<Client>> page(int page, int pageSize, String sortBy, String order, String filter) {
        return supplyAsync(() ->
                ebeanServer.find(Client.class).where()
                        .ilike("name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .setFirstRow(page * pageSize)
                        .setMaxRows(pageSize)
                        .findPagedList(), executionContext);
    }

    public CompletionStage<Optional<Client>> find(Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Client.class).setId(id).findOne()), executionContext);
    }

    public CompletionStage<Optional<Client>> insert(Client client) {
        return supplyAsync(() -> {
            try {
                ebeanServer.insert(client);
                return Optional.of(client);
            } catch (DuplicateKeyException e) {
                return Optional.empty();
            }
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> update(Long id, Client client) {
        return supplyAsync(() -> {
            Transaction txn = ebeanServer.beginTransaction();
            Optional<Long> value = Optional.empty();
            try {
                Client savedClient = ebeanServer.find(Client.class).setId(id).findOne();
                if (savedClient != null) {
                    savedClient.birthdate = client.birthdate;
                    savedClient.surname = client.surname;
                    savedClient.name = client.name;

                    savedClient.update();
                    txn.commit();
                    value = Optional.of(id);
                }
            } finally {
                txn.end();
            }
            return value;
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> delete(Long id) {
        return supplyAsync(() -> {
            try {
                final Optional<Client> optionalClient = Optional.ofNullable(ebeanServer.find(Client.class).setId(id).findOne());
                optionalClient.ifPresent(Model::delete);
                return optionalClient.map(c -> c.id);
            } catch (Exception e) {
                return Optional.empty();
            }
        }, executionContext);
    }

}
