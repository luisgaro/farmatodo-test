package models.repository;

import io.ebean.*;
import models.entity.Store;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class StoreRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public StoreRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    /**
     * Return a paged list of Store
     *
     * @param page     Page to display
     * @param pageSize Number of clients per page
     * @param sortBy   Store property used for sorting
     * @param order    Sort order (either or asc or desc)
     * @param filter   Filter applied on the name column
     */
    public CompletionStage<PagedList<Store>> page(int page, int pageSize, String sortBy, String order, String filter) {
        return supplyAsync(() ->
                ebeanServer.find(Store.class).where()
                        .ilike("name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .setFirstRow(page * pageSize)
                        .setMaxRows(pageSize)
                        .findPagedList(), executionContext);
    }

    public CompletionStage<Optional<Store>> find(Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Store.class).setId(id).findOne()), executionContext);
    }

    public CompletionStage<Optional<Store>> insert(Store client) {
        return supplyAsync(() -> {
            try {
                ebeanServer.insert(client);
                return Optional.of(client);
            } catch (DuplicateKeyException e) {
                return Optional.empty();
            }
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> update(Long id, Store client) {
        return supplyAsync(() -> {
            Transaction txn = ebeanServer.beginTransaction();
            Optional<Long> value = Optional.empty();
            try {
                Store savedStore = ebeanServer.find(Store.class).setId(id).findOne();
                if (savedStore != null) {
                    savedStore.name = client.name;
                    savedStore.hours = client.hours;
                    savedStore.address = client.address;

                    savedStore.update();
                    txn.commit();
                    value = Optional.of(id);
                }
            } finally {
                txn.end();
            }
            return value;
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> delete(Long id) {
        return supplyAsync(() -> {
            try {
                final Optional<Store> optionalStore = Optional.ofNullable(ebeanServer.find(Store.class).setId(id).findOne());
                optionalStore.ifPresent(Model::delete);
                return optionalStore.map(c -> c.id);
            } catch (Exception e) {
                return Optional.empty();
            }
        }, executionContext);
    }

}
