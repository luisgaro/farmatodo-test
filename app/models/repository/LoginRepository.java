package models.repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.entity.Client;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class LoginRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public LoginRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    public CompletionStage<Optional<Client>> login(Client client) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Client.class)
                .where()
                .eq("username", client.username)
                .eq("password", client.password)
                .findOne()), executionContext);
    }

}
