package models.repository;

import io.ebean.DuplicateKeyException;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.PagedList;
import models.entity.Purchase;
import models.entity.PurchaseDetail;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * A models.repository that executes database operations in a different
 * execution context.
 */
public class PurchaseRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public PurchaseRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    /**
     * Return a paged list of computer
     *
     * @param page     Page to display
     * @param pageSize Number of computers per page
     * @param sortBy   Computer property used for sorting
     * @param order    Sort order (either or asc or desc)
     * @param filter   Filter applied on the name column
     */
    public CompletionStage<PagedList<Purchase>> page(int page, int pageSize, String sortBy, String order, String filter) {
        return supplyAsync(() ->
                ebeanServer.find(Purchase.class).where()
                        .ilike("store.name", "%" + filter + "%")
                        .orderBy(sortBy + " " + order)
                        .fetch("store")
                        .setFirstRow(page * pageSize)
                        .setMaxRows(pageSize)
                        .findPagedList(), executionContext);
    }

    public CompletionStage<Optional<Purchase>> find(Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Purchase.class).setId(id).findOne()), executionContext);
    }

    public CompletionStage<Long> insert(Purchase purchase) {
        return supplyAsync(() -> {
            ebeanServer.insert(purchase);
            purchase.refresh();
            return purchase.id;
        }, executionContext);
    }

    public CompletionStage<Optional<PurchaseDetail>> insertDetail(PurchaseDetail purchaseDetail) {
        return supplyAsync(() -> {
            try {
                ebeanServer.insert(purchaseDetail);
                return Optional.of(purchaseDetail);
            } catch (DuplicateKeyException e) {
                return Optional.empty();
            }
        }, executionContext);
    }
}
