package models.presentation;

import models.interefaces.CreateRequired;
import play.data.validation.Constraints;

public class PurchaseDetail {

    @Constraints.Required(groups = CreateRequired.class)
    private Long productId;

    @Constraints.Required(groups = CreateRequired.class)
    private Double quantity;

    public PurchaseDetail() {
    }

    public PurchaseDetail(Long productId, Double quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
