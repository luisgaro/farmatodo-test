package models.presentation;

import models.interefaces.CreateRequired;
import play.data.validation.Constraints;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class Purchase {

    @Constraints.Required(groups = CreateRequired.class)
    private Long clientId;

    @Constraints.Required(groups = CreateRequired.class)
    private Long storeId;

    @Constraints.Required(groups = CreateRequired.class)
    @Valid
    private List<PurchaseDetail> products = new ArrayList<>();

    public Purchase() {
    }

    public Purchase(Long clientId, Long storeId, List<PurchaseDetail> products) {
        this.clientId = clientId;
        this.storeId = storeId;
        this.products = products;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public List<PurchaseDetail> getProducts() {
        return products;
    }

    public void setProducts(List<PurchaseDetail> products) {
        this.products = products;
    }
}
