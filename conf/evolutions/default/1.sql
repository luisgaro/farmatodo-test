# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table clients (
  id                            bigserial not null,
  name                          varchar(255) not null,
  surname                       varchar(255) not null,
  birthdate                     timestamptz,
  email                         varchar(255) not null,
  username                      varchar(255) not null,
  password                      TEXT not null,
  document_number               varchar(255) not null,
  constraint uq_clients_email unique (email),
  constraint uq_clients_username unique (username),
  constraint uq_clients_document_number unique (document_number),
  constraint pk_clients primary key (id)
);

create table products (
  id                            bigserial not null,
  name                          varchar(255) not null,
  description                   varchar(255),
  barcode                       TEXT not null,
  price                         float not null,
  constraint pk_products primary key (id)
);

create table purchases (
  id                            bigserial not null,
  client_id                     bigint not null,
  store_id                      bigint not null,
  create_date                   timestamptz,
  constraint pk_purchases primary key (id)
);

create table purchase_detail (
  id                            bigserial not null,
  product_id                    bigint not null,
  purchase_id                   bigint not null,
  quantity                      float,
  constraint pk_purchase_detail primary key (id)
);

create table stores (
  id                            bigserial not null,
  name                          varchar(255) not null,
  hours                         varchar(255) not null,
  address                       varchar(255) not null,
  constraint pk_stores primary key (id)
);

create table store_products (
  id                            bigserial not null,
  product_id                    bigint not null,
  store_id                      bigint not null,
  constraint pk_store_products primary key (id)
);

alter table purchases add constraint fk_purchases_client_id foreign key (client_id) references clients (id) on delete restrict on update restrict;
create index ix_purchases_client_id on purchases (client_id);

alter table purchases add constraint fk_purchases_store_id foreign key (store_id) references stores (id) on delete restrict on update restrict;
create index ix_purchases_store_id on purchases (store_id);

alter table purchase_detail add constraint fk_purchase_detail_product_id foreign key (product_id) references products (id) on delete restrict on update restrict;
create index ix_purchase_detail_product_id on purchase_detail (product_id);

alter table purchase_detail add constraint fk_purchase_detail_purchase_id foreign key (purchase_id) references purchases (id) on delete restrict on update restrict;
create index ix_purchase_detail_purchase_id on purchase_detail (purchase_id);

alter table store_products add constraint fk_store_products_product_id foreign key (product_id) references products (id) on delete restrict on update restrict;
create index ix_store_products_product_id on store_products (product_id);

alter table store_products add constraint fk_store_products_store_id foreign key (store_id) references stores (id) on delete restrict on update restrict;
create index ix_store_products_store_id on store_products (store_id);


# --- !Downs

alter table if exists purchases drop constraint if exists fk_purchases_client_id;
drop index if exists ix_purchases_client_id;

alter table if exists purchases drop constraint if exists fk_purchases_store_id;
drop index if exists ix_purchases_store_id;

alter table if exists purchase_detail drop constraint if exists fk_purchase_detail_product_id;
drop index if exists ix_purchase_detail_product_id;

alter table if exists purchase_detail drop constraint if exists fk_purchase_detail_purchase_id;
drop index if exists ix_purchase_detail_purchase_id;

alter table if exists store_products drop constraint if exists fk_store_products_product_id;
drop index if exists ix_store_products_product_id;

alter table if exists store_products drop constraint if exists fk_store_products_store_id;
drop index if exists ix_store_products_store_id;

drop table if exists clients cascade;

drop table if exists products cascade;

drop table if exists purchases cascade;

drop table if exists purchase_detail cascade;

drop table if exists stores cascade;

drop table if exists store_products cascade;

