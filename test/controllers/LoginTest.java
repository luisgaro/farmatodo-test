package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.entity.Client;
import org.junit.Assert;
import org.junit.Test;
import play.Application;
import play.Logger;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import play.test.WithApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static play.test.Helpers.*;

public class LoginTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    private Date formatted(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Test to validate the login method on api and LoginController when the data in the request is ok.
     */
    @Test
    public void loginTestOk() {

        Client client = new Client();
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";
        client.save();

        ObjectNode obj = Json.newObject();
        obj.put("username", "luis.gaviria");
        obj.put("password", "qwertyu123");
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(obj)
                .uri("/api/v1/login");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Logger.debug("Result: " + contentAsString(result));
        Logger.debug("jsonResult: " + jsonResult);
        Assert.assertEquals(OK, result.status());
        Assert.assertEquals(obj.get("username"), jsonResult.get("username"));

    }

    /**
     * Test to validate the login method on api and LoginController when the data in the request is bad,
     * because the username was not sent.
     */
    @Test
    public void loginTestFailUsernameIsRequired() {

        Client client = new Client();
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";
        client.save();

        ObjectNode obj = Json.newObject();
        //obj.put("username", "luis.gaviria");
        obj.put("password", "qwertyu123");
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(obj)
                .uri("/api/v1/login");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Logger.debug("Result: " + contentAsString(result));
        Logger.debug("jsonResult: " + jsonResult);
        Assert.assertEquals(BAD_REQUEST, result.status());
        Assert.assertEquals(jsonResult.get("username").get(0).textValue(), "This field is required");

    }

    /**
     * Test to validate the login method on api and LoginController when the data in the request is bad,
     * because the password of this username is wrong.
     */
    @Test
    public void loginTestFailUsernameOrPasswordIsWrong() {

        Client client = new Client();
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";
        client.save();

        ObjectNode obj = Json.newObject();
        obj.put("username", "luis.gaviria");
        obj.put("password", "aeiou");
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(obj)
                .uri("/api/v1/login");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Logger.debug("Result: " + contentAsString(result));
        Logger.debug("jsonResult: " + jsonResult);
        Assert.assertEquals(UNAUTHORIZED, result.status());
        Assert.assertEquals(jsonResult.asText(), "Username or password invalid!");

    }

}
