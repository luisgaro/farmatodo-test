package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.entity.Client;
import models.entity.Product;
import models.entity.Store;
import models.entity.StoreProducts;
import models.presentation.PurchaseDetail;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.Logger;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import play.test.WithApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static play.test.Helpers.*;

public class PurchaseTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    private Client client;
    private Product productA;
    private Product productB;
    private Store store;
    private StoreProducts storeProducts;

    private Date formatted(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    @Before
    public void setup() {

        client = new Client();
        client.id = 100L;
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";
        client.save();

        productA = new Product();
        productA.id = 100L;
        productA.name = "Product A";
        productA.description = "Description product A";
        productA.barcode = "ASD123";
        productA.price = 100D;
        productA.save();

        productB = new Product();
        productB.id = 101L;
        productB.name = "Product B";
        productB.description = "Description product B";
        productB.barcode = "ASD980";
        productB.price = 200D;
        productB.save();

        store = new Store();
        store.id = 100L;
        store.name = "Store Test";
        store.address = "Address test";
        store.hours = "Everyday from 8 to 18";
        store.save();

        storeProducts = new StoreProducts();
        storeProducts.id = 100L;
        storeProducts.product = productA;
        storeProducts.store = store;
        storeProducts.save();

        storeProducts.product = productB;
        storeProducts.id = 101L;
        storeProducts.store = store;
        storeProducts.save();


    }

    /**
     * Test to validate the create method on api and PurchaseController when the data in the request is ok.
     */
    @Test
    public void createTestOk() {

        List<PurchaseDetail> products = new ArrayList<>();
        ObjectNode objectNode = Json.newObject();
        objectNode.put("clientId", client.id);
        objectNode.put("storeId", store.id);

        products.add(new PurchaseDetail(productA.id, 10D));
        products.add(new PurchaseDetail(productB.id, 20D));

        objectNode.set("products", Json.toJson(products));

        Logger.debug("Req: " + objectNode);
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(objectNode)
                .uri("/api/v1/purchases");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Logger.debug("Result: " + jsonResult);
        Assert.assertEquals(OK, result.status());

    }

    @Test
    public void createTestFailStoreIdIsNotSent() {


        List<PurchaseDetail> products = new ArrayList<>();
        ObjectNode objectNode = Json.newObject();
        objectNode.put("clientId", client.id);
        //objectNode.put("storeId", store.id);

        products.add(new PurchaseDetail(productA.id, 10D));
        products.add(new PurchaseDetail(productB.id, 20D));

        objectNode.set("products", Json.toJson(products));

        Logger.debug("Req: " + objectNode);
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(objectNode)
                .uri("/api/v1/purchases");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Logger.debug("Result: " + jsonResult);
        Assert.assertEquals(BAD_REQUEST, result.status());
        Assert.assertEquals(jsonResult.get("storeId").get(0).textValue(), "This field is required");

    }


}
