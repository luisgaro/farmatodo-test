package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.Client;
import org.junit.Assert;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import play.test.WithApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static play.test.Helpers.*;

public class ClientTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    private Date formatted(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Test to validate the login method on api and LoginController when the data in the request is ok.
     */
    @Test
    public void createTestOk() {

        Client client = new Client();
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";

        JsonNode obj = Json.toJson(client);

        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(obj)
                .uri("/api/v1/clients");
        Result result = route(app, request);
        JsonNode jsonResult = Json.parse(contentAsString(result));
        Assert.assertEquals(OK, result.status());
        Assert.assertEquals(obj.get("username"), jsonResult.get("username"));

    }

    @Test
    public void createTestFailDuplicateEmail() {

        Client client = new Client();
        client.name = "Luis Hernando";
        client.surname = "Gaviria Roa";
        client.birthdate = formatted("1992-10-04");
        client.username = "luis.gaviria";
        client.password = "qwertyu123";
        client.documentNumber = "123456789";
        client.email = "luis.gaviria@email.com";
        client.save();

        Client clientRepeat = new Client();
        clientRepeat.name = "Luis Hernando";
        clientRepeat.surname = "Gaviria Roa";
        clientRepeat.birthdate = formatted("1992-10-04");
        clientRepeat.username = "luis.gaviria";
        clientRepeat.password = "qwertyu123";
        clientRepeat.documentNumber = "123456789";
        clientRepeat.email = "luis.gaviria@email.com";

        JsonNode obj = Json.toJson(clientRepeat);

        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .bodyJson(obj)
                .uri("/api/v1/clients");
        Result result = route(app, request);
        Assert.assertEquals(BAD_REQUEST, result.status());
    }

}
