name := "Farmatodo-test"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.11.11"

crossScalaVersions := Seq("2.11.11", "2.12.4")

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

libraryDependencies += "com.h2database" % "h2" % "1.4.196"
libraryDependencies ++= Seq(
  guice,
  jdbc,
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc4",
  "org.mindrot" % "jbcrypt" % "0.3m"
)

libraryDependencies += "org.awaitility" % "awaitility" % "2.0.0" % Test
libraryDependencies += "org.assertj" % "assertj-core" % "3.6.2" % Test
libraryDependencies += "org.mockito" % "mockito-core" % "2.1.0" % Test
javaOptions in Test += "-Dconfig.file=conf/application.test.conf"
testOptions in Test += Tests.Argument(TestFrameworks.JUnit, "-a", "-v")
